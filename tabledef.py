from sqlalchemy import *
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String, Time, Boolean, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
import datetime

engine = create_engine('sqlite:///datos.db', echo=True)
Base = declarative_base()

########################################################################
class User(Base):
    """"""
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String)
    password = Column(String)
    firstname = Column(String)
    lastname = Column(String)
    email = Column(String)
    organizer = Column(Boolean, unique=False, default=False)
    asked_for_organize = Column(Boolean, unique=False, default=False)
    admin = Column(Boolean, unique=False, default=False)
    alta_date = Column(Date)
    baja_date = Column(Date, default=None)
    events = relationship("Event", back_populates="creator")
    active = Column(Boolean, unique=False, default=True)

    #----------------------------------------------------------------------
    def __init__(self, username, password, firstname, lastname, email):
        """"""
        self.username = username
        self.password = password
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.alta_date = datetime.date.today()

########################################################################
class Event(Base):
    """"""
    __tablename__ = "events"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    date = Column(Date)
    start_time = Column(Time)
    end_time = Column(Time)
    venue = Column(String)
    fee = Column(Integer)
    limit = Column(Integer)
    description = Column(String)
    bracket = Column(String)
    game = Column(String)
    cover_image = Column(String)
    creator_id = Column(Integer, ForeignKey('users.id'))
    creator = relationship("User", back_populates="events")
    first = Column(String)
    second = Column(String)
    third = Column(String)
    fourth = Column(String)

class Game(Base):
    __tablename__ = 'games'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    cover_image = Column(String)
    alta_date = Column(Date)
    baja_date = Column(Date, default=None)
    active = Column(Boolean, unique=False, default=True)

    def __init__(self, name, url):
        self.name = name
        self.cover_image = url
        self.active = True
        self.alta_date = datetime.date.today()

class LogEntry(Base):
    __tablename__ = 'log'
    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime)
    doer = Column(String) 
    action = Column(String)
    target = Column(String) 

    def __init__(self, doer, action, target):
        self.timestamp = datetime.datetime.now()
        self.doer = doer
        self.action = action
        self.target = target


# create tables
Base.metadata.create_all(engine)
