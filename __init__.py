from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort, url_for, json
import os
from sqlalchemy.orm import sessionmaker
from tabledef import *
import hashlib
# import urllib2

# def urlencode(s):
#     return urllib2.quote(s)

# def urldecode(s):
#     return urllib2.unquote(s).decode('utf8')

engine = create_engine('sqlite:///datos.db', echo=True)

app = Flask(__name__)

def add_log_entry(doer=None, action=None, target=None):
    Session = sessionmaker(bind=engine)
    session = Session()
    entry = LogEntry(doer, action, target)
    session.add(entry)
    session.commit()

@app.route('/')
def home():
    Session = sessionmaker(bind=engine)
    s = Session()
    events = s.query(Event).all()
    # for e in events:
    #     print(e.name)

    if not session.get('logged_in'):
        return render_template('login_form.html', events=events)
    else:
        return render_template('logged_in.html', events=events)

@app.route('/register', methods=['GET'])
def register_show_form():
    if not session.get('logged_in'):
        return render_template('registration.html')
    else:
        return redirect(url_for('home'))

@app.route('/register', methods=['POST'])
def register():
    formdata = request.form
    given_username = formdata["username"]
    Session = sessionmaker(bind=engine)
    s = Session()
    query = s.query(User).filter(User.username.in_([given_username]))
    result = query.first()

    if result:
        return render_template('registrationerror.html')
    else:
        password_digest = hashlib.sha224(str(formdata["password"]).encode('utf-8')).hexdigest()
        #
        Session = sessionmaker(bind=engine)
        ss = Session()
        user = User(formdata["username"],password_digest,formdata["firstname"], formdata["lastname"], formdata["email"])
        ss.add(user)
        ss.commit()

        add_log_entry(user.username, "registered")

        return render_template('registered.html')


@app.route('/login', methods=['POST'])
def do_admin_login():

    POST_USERNAME = str(request.form['username'])
    POST_PASSWORD = hashlib.sha224(str(request.form['password']).encode('utf-8')).hexdigest()

    Session = sessionmaker(bind=engine)
    s = Session()
    query = s.query(User).filter(User.username.in_([POST_USERNAME]), User.password.in_([POST_PASSWORD]), User.active==True )
    result = query.first()
    if result:
        session['logged_in'] = True
        session['username'] = result.username
        session['organizer'] = result.organizer
        session['admin'] = result.admin
    else:
        session['logged_in'] = False
        return render_template('loginerror.html')
    return home()

@app.route('/profile', methods=['GET'])
def profile_show():
    if session.get('logged_in'):
        Session = sessionmaker(bind=engine)
        s = Session()
        query = s.query(User).filter(User.username == session['username'])
        user = query.first()
        print(user.__dict__)
        gravatar_hash = hashlib.md5(str(user.email).encode('utf-8')).hexdigest()
        return render_template('profile.html', user=user, gravatar_hash=gravatar_hash)
    else:
        return redirect(url_for('home'))

@app.route('/manage/users', methods=['GET'])
def manage_users_show():
    if session.get('logged_in'):
        Session = sessionmaker(bind=engine)
        s = Session()
        query = s.query(User).filter(User.active, User.username != session['username'])
        users = query.all()
        print(users)
        #gravatar_hash = hashlib.md5(str(user.email).encode('utf-8')).hexdigest()
        return render_template('manageusers.html', users=users)
        return
    else:
        return redirect(url_for('home'))

@app.route('/manage/users/remove/organizer/<user_id>', methods=['GET'])
def manage_users_remove_organizer(user_id):
    if session.get('logged_in'):
        Session = sessionmaker(bind=engine)
        s = Session()
        u = s.query(User).get(int(user_id))
        print(u.username)
        u.organizer = False
        s.commit()

        add_log_entry(session.get('username'), "remove_organizer", u.username)

        return redirect(url_for('manage_users_show'))
    else:
        return redirect(url_for('home'))

@app.route('/manage/users/grant/organizer/<user_id>', methods=['GET'])
def manage_users_grant_organizer(user_id):
    if session.get('logged_in'):
        Session = sessionmaker(bind=engine)
        s = Session()
        u = s.query(User).get(int(user_id))
        print(u.username)
        u.organizer = True
        u.asked_for_organize = False
        s.commit()

        add_log_entry(session.get('username'), "grant_organizer", u.username)

        return redirect(url_for('manage_users_show'))
    else:
        return redirect(url_for('home'))


@app.route('/manage/users/remove/admin/<user_id>', methods=['GET'])
def manage_users_remove_admin(user_id):
    if session.get('logged_in'):
        Session = sessionmaker(bind=engine)
        s = Session()
        u = s.query(User).get(int(user_id))
        print(u.username)
        u.admin = False
        s.commit()
        add_log_entry(session.get('username'), "remove_admin", u.username)
        return redirect(url_for('manage_users_show'))
    else:
        return redirect(url_for('home'))

@app.route('/manage/users/remove/user/<user_id>', methods=['GET'])
def manage_users_remove_user(user_id):
    if session.get('logged_in'):
        Session = sessionmaker(bind=engine)
        s = Session()
        u = s.query(User).get(int(user_id))
        print(u.username)
        u.admin = False
        u.organizer = False
        u.active = False
        import datetime
        u.baja_date = datetime.date.today()
        s.commit()
        add_log_entry(session.get('username'), "remove_user", u.username)
        return redirect(url_for('manage_users_show'))
    else:
        return redirect(url_for('home'))

@app.route('/manage/users/selfremove', methods=['GET'])
def manage_users_selfremove():
    if session.get('logged_in'):
        Session = sessionmaker(bind=engine)
        s = Session()
        u = s.query(User).filter(User.username == session['username']).first()
        print(u.username)
        u.admin = False
        u.organizer = False
        u.active = False
        import datetime
        u.baja_date = datetime.date.today()
        s.commit()
        add_log_entry(session.get('username'), "self_remove")
        return redirect(url_for('logout'))
    else:
        return redirect(url_for('home'))

@app.route('/manage/users/grant/admin/<user_id>', methods=['GET'])
def manage_users_grant_admin(user_id):
    if session.get('logged_in'):
        Session = sessionmaker(bind=engine)
        s = Session()
        u = s.query(User).get(int(user_id))
        print(u.username)
        u.admin = True
        s.commit()
        add_log_entry(session.get('username'), "grant_admin", u.username)
        return redirect(url_for('manage_users_show'))
    else:
        return redirect(url_for('home'))

@app.route('/ranking', methods=['GET'])
def ranking_show():
    Session = sessionmaker(bind=engine)
    s = Session()

    from sqlalchemy import text

    sql = text('select ranking.name, cover_image from (select game as name from events where game is not null group by game) as ranking, games where ranking.name = games.name and games.active = 1')
    result = engine.execute(sql)
    games = []
    for row in result:
        games.append({'name': row[0], 'cover_image': row[1]})

    # sql = text('select * from (select first as name, count(first) as first from events group by first) where name IS NOT NULL')
    # result = engine.execute(sql)
    # names = []
    # for row in result:
    #     print("-")
    #     print(row)

    if not session.get('logged_in'):
        return render_template('ranking.html', games=games, logged_in=False)
    else:
        return render_template('ranking.html', games=games, logged_in=True)

@app.route('/ranking/<game>', methods=['GET'])
def show_game_ranking(game):
    print("Evento: "+game)
    Session = sessionmaker(bind=engine)
    s = Session()

    from sqlalchemy import text

    ranking = {}
    # first
    query_string = ('select first as name, count(first) as first '
                    'from events where game = "'+game+'"'
                    'and first is not null '
                    'group by first')
    sql = text(query_string)
    result = engine.execute(sql)
    for row in result:
        ranking[row[0]] = int(row[1])*200
    # second
    query_string = ('select second as name, count(second) as second '
                    'from events where game = "'+game+'"'
                    'and second is not null '
                    'group by second')
    sql = text(query_string)
    result = engine.execute(sql)
    for row in result:
        if row[0] in result:
            ranking[row[0]] += int(row[1])*100
        else:
            ranking[row[0]] = int(row[1])*100
    # third
    query_string = ('select third as name, count(third) as third '
                    'from events where game = "'+game+'"'
                    'and third is not null '
                    'group by third')
    sql = text(query_string)
    result = engine.execute(sql)
    for row in result:
        if row[0] in result:
            ranking[row[0]] += int(row[1])*50
        else:
            ranking[row[0]] = int(row[1])*50
    # fourth
    query_string = ('select fourth as name, count(fourth) as fourth '
                    'from events where game = "'+game+'"'
                    'and fourth is not null '
                    'group by fourth')
    sql = text(query_string)
    result = engine.execute(sql)
    for row in result:
        if row[0] in result:
            ranking[row[0]] += int(row[1])*25
        else:
            ranking[row[0]] = int(row[1])*25
    import operator
    sorted_ranking = sorted(ranking.items(), key=operator.itemgetter(1))
    sorted_ranking.reverse()
    print(sorted_ranking)

    if not session.get('logged_in'):
        return render_template('ranking_game.html', logged_in=False, ranking=sorted_ranking, game=game)
    else:
        return render_template('ranking_game.html', logged_in=True, ranking=sorted_ranking, game=game)

@app.route('/organizer_apply', methods=['POST'])
def organizer_apply():
    if session.get('logged_in'):
        # Session = sessionmaker(bind=engine)
        # s = Session()
        # query = s.query(User).filter(User.username == session['username'])
        # user = query.first()
        # print(user.__dict__)
        # gravatar_hash = hashlib.md5(str(user.email).encode('utf-8')).hexdigest()
        formdata = request.form
        print(formdata)
        Session = sessionmaker(bind=engine)
        s = Session()
        query = s.query(User).filter(User.username == formdata['username'])
        user = query.first()
        user.asked_for_organize = True
        s.commit()
        add_log_entry(user.username, "organizer_apply")

        return render_template('application_submitted.html')
    else:
        return redirect(url_for('home'))

@app.route('/event/new', methods=['GET'])
def new_event():
    if session.get('logged_in') and (session.get('organizer') or session.get('admin')):
        Session = sessionmaker(bind=engine)
        s = Session()
        query = s.query(Game).filter(Game.active)
        games = query.all()
        return render_template('new_event.html', games=games)
    else:
        return redirect(url_for('home'))

@app.route('/event/<id>', methods=['GET'])
def show_event(id):
    print("Evento: "+id)
    Session = sessionmaker(bind=engine)
    s = Session()
    event = s.query(Event).get(id)
    if not event:
        return redirect(url_for('home'))
    else:
        if session.get('logged_in'):
            base = 'logged_in.html'
        else:
            base = 'login_form.html'
        return render_template('event_info.html', event=event, base=base)
    print(event)

@app.route('/event/bracket/<id>', methods=['GET'])
def create_bracket(id):
    Session = sessionmaker(bind=engine)
    s = Session()
    event = s.query(Event).get(id)
    if not event:
        return redirect(url_for('home'))
    else:
        if session.get('logged_in') and (session['organizer'] or session['admin']):
            base = 'logged_in.html'
            return render_template('create_bracket.html', event=event, base=base)
        else:
            return redirect(url_for('home'))
        
    print(event)


@app.route('/event/bracket/<id>', methods=['POST'])
def save_bracket(id):
    formdata = request.form
    bracketData = request.form['bracket']
    results = request.form['results']
    results_arr = json.loads(results)
    print(results_arr)
    Session = sessionmaker(bind=engine)
    s = Session()
    event = s.query(Event).get(id)
    event.bracket = bracketData
    event.first = results_arr[0]
    event.second = results_arr[1]
    event.third = results_arr[2]
    event.fourth = results_arr[3]
    s.commit()
    add_log_entry(session.get('username'), "bracket_updated", event.name)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 


@app.route('/event/new', methods=['POST'])
def do_create_event():
    if session.get('logged_in'):
        Session = sessionmaker(bind=engine)
        s = Session()
        # query = s.query(User).filter(User.username == session['username'])
        # user = query.first()
        # print(user.__dict__)
        # gravatar_hash = hashlib.md5(str(user.email).encode('utf-8')).hexdigest()

        # ImmutableMultiDict([('address', ''), ('games', 'Smashito'), ('games', 'Marvelito'),
        # ('venue', ''), ('fee', ''), ('limit', ''), ('end_time', ''), ('event_name', ''),
        # ('description', ''), ('date', ''), ('cover_image', ''), ('start_time', '')])

        formdata = request.form
        event = Event()
        event.name = formdata.get('event_name')
        event.bracket = '{"teams":[["Jugador 1","Jugador 2"],["Jugador 3","Jugador 4"]],"results":[[[[null,null],[null,null]],[[null,null],[null,null]]]]}'

        from dateutil.parser import parse
        event.date = parse(formdata.get('date')).date()
        event.start_time = parse(event.date.strftime('%d/%m/%Y')+" "+formdata.get("start_time")).time()
        event.end_time = parse(event.date.strftime('%d/%m/%Y')+" "+formdata.get("end_time")).time()
        event.venue = formdata.get("venue")
        event.fee = int(formdata.get("fee"))
        event.limit = int(formdata.get("limit"))
        event.description = formdata.get("description")
        event.cover_image = formdata.get("cover_image")
        event.game = formdata.get("game")
        
        query = s.query(User).filter(User.username == session['username'])
        user = query.first()

        event.creator = user

        s.add(event)
        s.commit()

        print("Form data: ")
        print(formdata)
        add_log_entry(session.get('username'), "event_created", event.name)
        return render_template('event_created.html')
    else:
        return redirect(url_for('home'))

@app.route('/manage/games', methods=['GET'])
def manage_games():
    if session.get('logged_in') and session.get('admin'):
        Session = sessionmaker(bind=engine)
        s = Session()
        query = s.query(Game).filter(Game.active)
        games = query.all()
        print(games)
        #gravatar_hash = hashlib.md5(str(user.email).encode('utf-8')).hexdigest()
        

        return render_template('managegames.html', games=games)
    else:
        return redirect(url_for('home'))

@app.route('/game/disable/<game_id>', methods=['GET'])
def manage_game_disable(game_id):
    if session.get('logged_in') and session.get('admin'):
        Session = sessionmaker(bind=engine)
        s = Session()
        g = s.query(Game).get(int(game_id))
        g.active = False
        s.commit()
        add_log_entry(session.get('username'), "game_disabled", g.name)
        return redirect(url_for('manage_games'))
    else:
        return redirect(url_for('home'))

@app.route('/game/new', methods=['POST'])
def add_new_game():
    formdata = request.form
    game_name = formdata["game_name"]
    game_cover_url = formdata["game_image"]

    Session = sessionmaker(bind=engine)
    s = Session()
    game = Game(game_name,game_cover_url)
    s.add(game)
    s.commit()
    add_log_entry(session['username'], "game_created", game_name)
    return redirect(url_for('manage_games'))

@app.route('/log', methods=['GET'])
def show_log():
    if session.get('logged_in') and session['admin']:
        Session = sessionmaker(bind=engine)
        s = Session()
        query = s.query(LogEntry).order_by(LogEntry.timestamp.desc())
        entries = query.all()

        parsed_entries = []

        for entry in entries:
            msg = ""
            if entry.action == "registered":
                msg = "El usuario "+entry.doer+" se ha registrado."
            elif entry.action == "remove_organizer":
                msg = entry.doer+" ha quitado permisos de organizador a "+entry.target+"."
            elif entry.action == "grant_organizer":
                msg = entry.doer+" ha otorgado permisos de organizador a "+entry.target+"."
            elif entry.action == "remove_admin":
                msg = entry.doer+" ha quitado permisos de administrador a "+entry.target+"."
            elif entry.action == "remove_user":
                msg = entry.doer+" ha dado de baja la cuenta de "+entry.target+"."
            elif entry.action == "self_remove":
                msg = "El usuario "+entry.doer+" ha dado de baja su cuenta."
            elif entry.action == "grant_admin":
                msg = entry.doer+" ha otorgado permisos de administrador a "+entry.target+"."
            elif entry.action == "organizer_apply":
                msg = "El usuario "+entry.doer+" ha solicitado ser organizador."
            elif entry.action == "bracket_updated":
                msg = "El organizador "+entry.doer+" ha actualizado el bracket del evento "+entry.target+"."
            elif entry.action == "event_created":
                msg = "El organizador "+entry.doer+" ha creado el evento "+entry.target+"."
            elif entry.action == "game_disabled":
                msg = entry.doer+" ha dado de baja el juego "+entry.target+"."
            elif entry.action == "game_created":
                msg = entry.doer+" ha ingresado el juego "+entry.target+"."
            else:
                msg = entry.doer + " " + entry.action + " " + entry.target
            parsed_entries.append({'timestamp':entry.timestamp.strftime('%Y-%m-%d a las %H:%M'), 'msg':msg})
            
        

        return render_template('show_log.html', entries=parsed_entries)
    else:
        return redirect(url_for('home'))

@app.route("/logout")
def logout():
    session['logged_in'] = False
    return redirect(url_for('home'))

if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(debug=True,host='0.0.0.0', port=5000)
